var net = require("net")
var strftime = require("strftime")
var server = net.createServer(listener)
server.listen(process.argv[2])

function listener(socket) {
	var date = strftime("%F %H:%M");
	socket.write(date);
	socket.end("\n");
}