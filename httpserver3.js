var http = require("http")
var fs = require("fs")
var url = require("url")
var server = http.createServer(function(req, res) {
	if(req.method == "GET") {
		var requestUrl = url.parse(req.url, true)
		var date = new Date(requestUrl.query.iso)
		//console.log(requestUrl)
		var body = ''
		switch(requestUrl.pathname) {
			case '/api/unixtime' :
				body = unixTime(date)
				break;
			case '/api/parsetime' :
				body = parseTime(date)
				break;
		}
		res.writeHead(200, { 'Content-Type': 'application/json' })
		res.end(body)
	}
})
server.listen(process.argv[2])

function parseTime(date) {
	return JSON.stringify({
		hour : date.getHours(),
		minute : date.getMinutes(),
		second : date.getSeconds()
	})
}

function unixTime(date) {
	return JSON.stringify({
		unixtime : date.valueOf()
	})
}