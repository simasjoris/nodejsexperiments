var fs = require("fs")
var pathTools = require("path")
module.exports = function(path, extension, callback) {
	fs.readdir(path, function(err, list) {
		if(err)
			return callback(err)
		var data = []
		for(var i =0; i < list.length; i++) {
			//console.log(pathTools.extname(list[i]))
			if(pathTools.extname(list[i]) == '.' + extension) {
				data.push(list[i])
				//console.log(list[i])
			}
		}
		callback(null, data)
	})
}
